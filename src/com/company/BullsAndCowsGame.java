package com.company;

import java.io.*;
import java.security.Key;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class BullsAndCowsGame {

    public int[] computerSecretCode = new int[4];
    public int[] playerSecretCode = new int[4];
    public int[] playerGuessArray = new int[4];
    public int[] computerGuessArray = new int[4];

    private Computer c1 = new Computer();
    private Player p1 = new Player();

    private String playerChoice;
    private String playType;
    private int round = 1;
    String gameResult;

    ArrayList<int[]> collection = new ArrayList<>();
    ArrayList<int[]> resultHistory = new ArrayList<>();

    public EasyAI easy1 = new EasyAI();
    public MediumAI medium1 = new MediumAI();
    public HardAI hard1 = new HardAI();

    private File myFile;


    //the program starts here
    public void start() {

        System.out.println("Please choose Easy, Medium or Hard (E/M/H): ");
        this.playerChoice = Keyboard.readInput();

        System.out.println("Would you like to play manually or automatic (M/A): ");
        this.playType = Keyboard.readInput();

        if (this.playType.equals("A")) {
            generateFile();
        }

        this.computerSecretCode = c1.generateFourRandomDigits();
        System.out.println("Please enter your secret code: ");
        this.playerSecretCode = askUser();
        System.out.println("---");
        System.out.println("Computer Code (cheat): ");
        for (int i = 0; i < this.computerSecretCode.length; i++) {
            System.out.print(this.computerSecretCode[i]);
        }
        System.out.println("---");

        this.playGame();

    }

    //method to ask user to enter a file name until it is found
    public void generateFile(){

        System.out.println("Please enter a file name: ");
        String fileName = Keyboard.readInput();

        this.myFile = new File(fileName + ".txt");

        if (!this.myFile.exists()){
            generateFile();
        }

    }

    // method to play the game, this will gets repeated as long as the game lasts
    public void playGame() {

        System.out.println("Round: " + this.round);

        //code for dealing with automatic options, reading from the specified file then assign player's guess accordingly
        if (playType.equals("A")) {
            String answerFromFile = "";
            boolean check = true;
            try (Scanner s1 = new Scanner(new FileReader(myFile))) {
                for (int i = 0; i < round; i++) {
                    if (s1.hasNext()) {
                        answerFromFile = s1.next();
                        check = true;
                    }
                    else {
                        System.out.println("Your guess? - ");
                        this.playerGuessArray = askUser();
                        check = false;
                        i=round;
                    }
                }
                if (check){
                for (int i = 0; i < 4; i++) {
                    this.playerGuessArray[i] = answerFromFile.charAt(i) - '0';
                }
                System.out.println("Your guess: " + answerFromFile);
                }
            }
            catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }

        // else, manual option - prompt user to enter a value
        else {
            System.out.println("Your guess");
            this.playerGuessArray = askUser();
        }

        int playerBulls = p1.getNumberOfBulls(computerSecretCode, playerGuessArray);
        int playerCows = p1.getNumberOfCows(computerSecretCode, playerGuessArray);
        int playerGuessInt = convertArrayToInt(playerGuessArray);

        // prints user result
        System.out.println("Result: " + playerBulls + " bulls | " + playerCows + " cows");

        // code to deal with Easy AI - generate a random guess
        if (playerChoice.equals("E")) {

            this.computerGuessArray = easy1.generateGuess();
        }

        //code to deal with Medium AI - generate random, unrepeated guess
        else if (playerChoice.equals("M")) {
            this.computerGuessArray = medium1.generateGuess(collection);
            collection.add(this.computerGuessArray);
        }

        //code to deal with Hard AI - generate a strategized guess
        else if (playerChoice.equals("H")) {

            this.computerGuessArray = hard1.generateGuess();

            System.out.println("SIZE BEFORE: " + hard1.possibleAnswers.size());

            System.out.println("BULLS ____  " + hard1.getNumberOfBulls(this.computerGuessArray, this.playerSecretCode));

            if (hard1.getNumberOfBulls(this.computerGuessArray, this.playerSecretCode) > 0) {
                hard1.removeImpossibleAnswers(this.computerGuessArray);
            }

            else {
                hard1.removeZeroBulls(this.computerGuessArray);
            }

            System.out.println("SIZE AFTER: " + hard1.possibleAnswers.size());
        }

        int computerBulls = c1.getNumberOfBulls(playerSecretCode, computerGuessArray);
        int computerCows = c1.getNumberOfCows(playerSecretCode, computerGuessArray);
        int computerGuessInt = convertArrayToInt(computerGuessArray);


        //prints computer guess and result
        printComputerGuess(this.computerGuessArray);
        System.out.println("Result: " + computerBulls + " bulls | " + computerCows + " cows");

        //storing result history into a collection for later use
        int[] resultArray = new int[]{round,playerGuessInt,playerBulls,playerCows,computerGuessInt,computerBulls,computerCows};
        resultHistory.add(resultArray);

        //check who is the winner, this will call back the playGame() method if no winner is found until the game ends as a draw
        checkWinner();

    }

    // assisting method to print out computer's guess from an array
    public void printComputerGuess(int[] a) {
        System.out.println("Computer guess: ");
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i]);
        }
        System.out.println();
    }

    //prompt user to enter a guess and check if the guess is valid, returns an array of 4 different integers
    public int[] askUser() {

        String playerGuessString = Keyboard.readInput();

        //check if the input value was integers
        while (!checkIfNumeric(playerGuessString)) {
            System.out.println("Invalid answer. Please enter an integer:");
            playerGuessString = Keyboard.readInput();
        }

        //check if the input value was correct in length
        while (playerGuessString.length() != 4) {
            System.out.println("Invalid answer. Please enter an answer of 4 digits:");
            playerGuessString = Keyboard.readInput();
        }

        //check if the input value was in correct format
        while (!checkIfCorrectFormat(playerGuessString)) {
            System.out.println("Invalid answer. Please enter the correct format:");
            playerGuessString = Keyboard.readInput();
        }

        //convert the answer from string to array
        int[] userInputArray = new int[4];
        for (int i = 0; i < 4; i++) {
            userInputArray[i] = playerGuessString.charAt(i) - '0';
        }

        return userInputArray;

    }

    // assisting method to check if a string is numeric
    public boolean checkIfNumeric(String a) {
        try {
            Integer.parseInt(a);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    // assisting method to check is a string is in correct format (no duplicated index)
    public boolean checkIfCorrectFormat(String a) {
        for (int i = 0; i < a.length(); i++) {
            for (int x = 0; x < a.length(); x++) {
                if ((i != x) && (a.charAt(i) == a.charAt(x))) {
                    return false;
                }
            }
        }
        return true;
    }

    // method to check if the user or the computer won the game, call back playGame() until the game ends as a draw
    public void checkWinner() {

        while (round < 99) {

            if (p1.getNumberOfBulls(computerSecretCode, playerGuessArray) == 4) {
                System.out.println("Player Won");
                this.round = 8;
                gameResult = "player";
                gameEnds();

            } else if (c1.getNumberOfBulls(playerSecretCode, computerGuessArray) == 4) {
                System.out.println("Computer Won");
                this.round = 8;
                gameResult = "computer";
                gameEnds();

            } else {
                if (round == 7) {
                    System.out.println("it's a draw!");
                    this.round = 8;
                    gameResult = "draw";
                    gameEnds();

                } else {
                    round++;
                    playGame();
                }
            }
        }
    }

    // assisting method to convert an array to an int
    public int convertArrayToInt(int[] array){

        String convertedString = "";
        for (int i = 0; i < array.length; i++){
            convertedString = convertedString + array[i];
        }
        int convertedInt = Integer.parseInt(convertedString);

        return convertedInt;
    }

    // method to print result out to a file after the game ends
    public void gameEnds(){
        System.out.println("Would you like to print out the result? (Y/N)");
        String userPrintAnswer = Keyboard.readInput();

        if (userPrintAnswer.equals("N")){
            System.out.println("bye bye");
        }
        else if (userPrintAnswer.equals("Y")){

            System.out.println("Please enter a file name");
            File myFile = new File(Keyboard.readInput() + ".txt");

            try(PrintWriter pw1 = new PrintWriter(new FileWriter(myFile))){
                pw1.println("Bull and cows game result");
                pw1.println("Your secret code: " + convertArrayToInt(this.playerSecretCode));
                pw1.println("Computer secret code: " + convertArrayToInt(this.computerSecretCode));
                pw1.println("-----------");

                for (int i = 0; i < resultHistory.size(); i++){
                    pw1.println("Round: " + resultHistory.get(i)[0]);
                    pw1.println("Player Chose: " + resultHistory.get(i)[1]);
                    pw1.println("Result: " + resultHistory.get(i)[2] + " Bulls | " + resultHistory.get(i)[3] + " Cows.");
                    pw1.println("Computer chose: " + resultHistory.get(i)[4]);
                    pw1.println("Result: " + resultHistory.get(i)[5] + " Bulls | " + resultHistory.get(i)[6] + " Cows.");
                    pw1.println("---------");
                }

                if (gameResult.equals("player")){
                    pw1.println("Player Won");
                }
                else if (gameResult.equals("computer")){
                    pw1.println("Computer Won");
                }
                else if (gameResult.equals("draw")){
                    pw1.println("It's a draw!");
                }

            }
            catch (IOException e){
                System.out.println(e.getMessage());
            }
        }

    }


    public static void main(String[] args) {
        //start the program
        BullsAndCowsGame game = new BullsAndCowsGame();
        game.start();
    }

}
