package com.company;

import java.util.ArrayList;

public class MediumAI extends Computer {

    //generate an array of 4 random digits as long as it does not match an existing array in the collection
    public int[] generateGuess(ArrayList<int[]> mediumCollection) {
        int[] guessArray = generateFourRandomDigits();
        while (checkCollection(mediumCollection,guessArray)){
            guessArray = generateFourRandomDigits();
        }
        return guessArray;
    }

    // a helper method - return true if the two arrays are the same, false otherwise
    public boolean checkIfSame(int[] array1, int[] array2) {
        for (int a = 0; a < array1.length; a++) {
            if (array1[a] != array2[a]) {
                return false;
            }
        }
        return true;
    }

    // a helper method - return true if the array is in collection, false otherwise
    public boolean checkCollection(ArrayList<int[]> mediumCollection, int[] guessArray){

        for (int[] array : mediumCollection) {
            if (checkIfSame(array, guessArray)) {
               return true;
            }
        }

        return false;
    }
}
