package com.company;

import java.util.ArrayList;
import java.util.Collections;

public class Computer {

    //generate an array of 4 different integers between 0-9 (inclusive)
    public int[] generateFourRandomDigits() {

        ArrayList<Integer> listOfNumbers = new ArrayList<>(10);

        for (int i = 0; i < 10; i++){
            listOfNumbers.add(i);
        }

        Collections.shuffle(listOfNumbers);

        int[] fourDigitsArray = new int[4];

        for (int i = 0; i < 4; i++){
            fourDigitsArray[i] = listOfNumbers.get(i);

        }

        return fourDigitsArray;
    }

    public int getNumberOfBulls (int[] userSecretCode, int[] computerGuessArray){
        int bulls = 0;

        for (int i = 0; i < userSecretCode.length; i++){
            if (computerGuessArray[i] == userSecretCode[i]){
                bulls++;
            }
        }

        return bulls;
    }

    public int getNumberOfCows(int[] userSecretCode, int[] computerGuessArray){
        int cows = 0;

        for (int x = 0; x < userSecretCode.length; x++){
            for (int y = 0; y < computerGuessArray.length; y++){
                if (computerGuessArray[y] == userSecretCode[x]){
                    cows++;
                }
            }
        }

        cows = cows - getNumberOfBulls(userSecretCode,computerGuessArray);

        return cows;
    }
}
