package com.company;

public class Player {

    public int getNumberOfBulls (int[] secretCode, int[] guessArray){
        int bulls = 0;

        for (int i = 0; i < secretCode.length; i++){
            if (guessArray[i] == secretCode[i]){
                bulls++;
            }
        }

        return bulls;
    }

    public int getNumberOfCows(int[] secretCode, int[] guessArray){
        int cows = 0;

        for (int x = 0; x < secretCode.length; x++){
            for (int y = 0; y < guessArray.length; y++){
                if (secretCode[y] == guessArray[x]){
                    cows++;
                }
            }
        }

        cows = cows - getNumberOfBulls(secretCode,guessArray);

        return cows;
    }

}
