package com.company;

import org.omg.CORBA.PUBLIC_MEMBER;

import java.util.ArrayList;

public class HardAI extends Computer {

    public ArrayList<int[]> possibleAnswers = new ArrayList<>();

    public HardAI(){
        this.generateListOfPossibleAnswers();
    }

    public int[] generateGuess() {

        int[] a = this.possibleAnswers.get((int)(Math.random()*possibleAnswers.size()));

        return a;
    }

    public void generateListOfPossibleAnswers() {

        while (possibleAnswers.size() < 5040) {
            int[] a = generateFourRandomDigits();

            if (!checkCollection(possibleAnswers, a)) {
                possibleAnswers.add(a);
            }

        }

    }


    // a helper method - return true if the two arrays are the same, false otherwise
    public boolean checkIfSame(int[] array1, int[] array2) {
        for (int a = 0; a < array1.length; a++) {
            if (array1[a] != array2[a]) {
                return false;
            }
        }
        return true;
    }

    // a helper method - return true if the array is in collection, false otherwise
    public boolean checkCollection(ArrayList<int[]> collection, int[] array) {

        for (int[] a : collection) {
            if (checkIfSame(a, array)) {
                return true;
            }
        }

        return false;
    }

    public void removeZeroBulls(int[] guess){
        int count;

        for (int i = 0; i < possibleAnswers.size(); i++){
            for (int a = 0; a < 4; a++){
                count = 1;
                if (possibleAnswers.get(i)[a] == guess[a]){
                    count++;
                    if (count == 4){
                        possibleAnswers.remove(i);
                    }
                }
            }
        }
    }

    //check for possible answers, remove impossibles from the list
    public void removeImpossibleAnswers(int[] guess) {
        boolean removeStatus = true;
        int count;

        for (int i = 0; i < possibleAnswers.size(); i++){

            for (int a = 0; a < 4; a++){
                 removeStatus = true;
                 count = 1;
                if (possibleAnswers.get(i)[a] == guess[a]){
                    removeStatus = false;
                    count++;
                    if (count == 4){
                        possibleAnswers.remove(i);
                    }
                }
            }

            if (removeStatus){
                possibleAnswers.remove(i);
            }

        }








//            for (int i = 0; i < possibleAnswers.size(); i++) {
//
//                for (int a = 0; a < 4; a++){
//                   removeStatus = true;
//                    if (guess[0] != possibleAnswers.get(i)[a]){
//                        if (guess[1] != possibleAnswers.get(i)[a]){
//                            if (guess[2] != possibleAnswers.get(i)[a]){
//                                if (guess[3] != possibleAnswers.get(i)[a]){
//                                    removeStatus = false;
//                                }
//                            }
//                        }
//                    }
//                }
//
//                if (removeStatus){
//                    possibleAnswers.remove(i);
//                }
//            }
        }



}
